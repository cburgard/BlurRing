## BlurRing

BlurRing is an implementation of multi-dimensional likelihood contour
sampling using Markov chains. It was developed in the context of high
energy physics research, but can be used for visualization of any
multi-dimensional likelihood. The first proof-of-concept
implementation was done using the `RooFit` statistics suite from the
data analysis package `ROOT`, but other implementations will be added
over time.

# Setup

Setup some version of ROOT. Don't forget to make sure that `CMake` can find your compiler version

    export CC=$(which gcc) 
    export CXX=$(which g++)

Then, build the package

    mkdir build
    cd build
    cmake ..
    make -j8
    
Now, you can run the demo

    ./bin/demo
