#ifndef __BlurRing__GeneralFunctionSampler__hpp__
#define __BlurRing__GeneralFunctionSampler__hpp__
#include "BlurRing/Sampler.hpp"

class RooAbsReal;

namespace BlurRing {

  class GeneralFunctionSampler : public BlurRing::Sampler{
  protected:
    RooAbsReal* func;

    const double funcmin = 0;
    const double funcmax = 2;
    const bool logarithmic = true;

    double getMinimumFunctionValue() const;    
    double getRandomFunctionValue() const;
    virtual void initialize();
    
  public:
    GeneralFunctionSampler(RooAbsReal* f,
            const std::vector<RooRealVar*>& vars,
            double fmax = 2,
            bool log = true,
            double b = -1);
    virtual ~GeneralFunctionSampler();    
  };

}
#endif
