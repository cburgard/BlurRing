#ifndef __BlurRing__RejectionSampler__hpp__
#define __BlurRing__RejectionSampler__hpp__
#include "BlurRing/GeneralFunctionSampler.hpp"

namespace BlurRing {

  class RejectionSampler : public BlurRing::GeneralFunctionSampler {

  protected:
    void randomizeVars() const ;

  public:
    using GeneralFunctionSampler::GeneralFunctionSampler;
    virtual ~RejectionSampler();    

    virtual bool generate() const override;

  };
  
}

#endif
