#ifndef __BlurRing__GibbsSampler__hpp__
#define __BlurRing__GibbsSampler__hpp__
#include "BlurRing/GeneralFunctionSampler.hpp"
#include "BlurRing/RejectionSampler.hpp"

namespace BlurRing {

  class GibbsSampler : public BlurRing::GeneralFunctionSampler {

  protected:
    std::map<RooRealVar*,BlurRing::RejectionSampler> samplers;
    
    void randomizeVars() const ;
    void initialize() override ;

    int skip = -1;
    
  public:
    using GeneralFunctionSampler::GeneralFunctionSampler;
    virtual ~GibbsSampler();

    void configure(int burnIn, int _skip);
    
    virtual bool generate() const override;

  };
  
}

#endif
