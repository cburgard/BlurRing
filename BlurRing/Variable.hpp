#ifndef __BlurRing__Variable__hpp__
#define __BlurRing__Variable__hpp__
class RooRealVar;

namespace BlurRing {
  class Variable {
  protected:
    mutable RooRealVar* var;
    double minVal;
    double min;
    double max;
    double step;
    double error;
  public:
    Variable(RooRealVar*  _var, double _min, double _max, double _step);
    Variable(RooRealVar*  _var, double _step);
    Variable(RooRealVar*  _var);
    Variable();    
      
    void toMinimum() const;
    double getMinimum() const;
    double getMin() const;
    double getMax() const;
    double getStep() const;
    double getError() const;    
    RooRealVar* getVar() const; 
  };
}
#endif
