#ifndef __BlurRing__GaussSampler__hpp__
#define __BlurRing__GaussSampler__hpp__

#include "BlurRing/Sampler.hpp"

namespace BlurRing {

  class GaussSampler : public BlurRing::Sampler {
  public:
    GaussSampler(const std::vector<RooRealVar*>& vars,double b = -1);
    
    virtual bool generate() const override ;
  };
  
}
#endif
