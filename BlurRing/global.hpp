#ifndef __BlurRing__global__hpp__
#define __BlurRing__global__hpp__
#include <vector>

class TGraph;

class RooAbsCollection;
class RooAbsReal;

#include <BlurRing/Variable.hpp>
#include <BlurRing/Sampler.hpp>

namespace BlurRing {

  extern unsigned int max_failrate; // maximum number of failed samples per requested sample
  extern unsigned int max_failcount; // maximum number of failed samples in total

  std::vector<std::vector<TGraph*>> getContourSet(RooAbsReal* func,double offset,
                                                  const BlurRing::Variable& x,
                                                  const BlurRing::Variable& y,
                                                  const std::vector<double>& contourVals);

  std::vector<std::vector<TGraph*>> getContours(RooAbsReal* func,const BlurRing::Sampler& sampler,
                                                const BlurRing::Variable& x,
                                                const BlurRing::Variable& y,
                                                const std::vector<double>& contourVals, int nSamples, bool relNorm=false);

  std::vector<std::vector<TGraph*>> getGaussianContours(RooAbsReal* func,RooAbsCollection* args,
                                                        const BlurRing::Variable& x,
                                                        const BlurRing::Variable& y,
                                                        const std::vector<double>& contourVals, int nSamples, bool relNorm=false, 
                                                        double box = -1, unsigned int seed = 0);
  
  std::vector<std::vector<TGraph*>> getContours(RooAbsReal* func,RooAbsCollection* args,
                                                const BlurRing::Variable& x,
                                                const BlurRing::Variable& y,
                                                const std::vector<double>& contourVals, int nSamples = 1000, bool relNorm=false,
                                                double funcmax = 2, bool logarithmic = true, double box = -1,unsigned int seed=0);
  
}
#endif
