#ifndef __BlurRing__Sampler__hpp__
#define __BlurRing__Sampler__hpp__

#include <map>
#include <random>

class RooRealVar;

namespace BlurRing {

  class Sampler {
  protected:
    Sampler(const std::vector<RooRealVar*>& vars, double b);

    mutable std::default_random_engine random;
    
    std::map<RooRealVar*,double> variables;
    const double box = -1;

    void fillRandomNormVector(std::map<RooRealVar*,double>& vec) const ;
    
  public:
    void setSeed(int seed);
    void printVars() const;
    void setConst(bool constval) const;
    
    virtual bool generate() const = 0;
  };

}
#endif
