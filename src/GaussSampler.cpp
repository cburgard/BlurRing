#include "BlurRing/GaussSampler.hpp"

#include "RooRealVar.h"

namespace BlurRing {

  GaussSampler::GaussSampler(const std::vector<RooRealVar*>& vars, double b) : Sampler(vars,b) {}
  
  bool GaussSampler::generate() const {
    std::map<RooRealVar*,double> vals;
    fillRandomNormVector(vals);
    std::chi_squared_distribution<double> chi2(this->variables.size());
    double rad = chi2(this->random);
    for(auto& var:this->variables){
      double range = (box <= 0 ? std::min(fabs(var.second - var.first->getMin()),fabs(var.second - var.first->getMax())) : box*var.first->getError() );
      var.first->setVal(vals[var.first]*range*rad + var.second);
    }
    return true;
  }
}


