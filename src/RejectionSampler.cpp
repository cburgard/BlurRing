#include "BlurRing/RejectionSampler.hpp"

#include "RooAbsReal.h"
#include "RooRealVar.h"

namespace BlurRing {

  RejectionSampler::~RejectionSampler(){
    // default destructor
  }
  bool RejectionSampler::generate() const {
    // generate a sample
    this->randomizeVars();
    double randfuncval = this->getRandomFunctionValue();
    double funcval = func->getVal()-this->getMinimumFunctionValue();
    if(funcval < randfuncval){
      // if the sample is not accepted, retry
      return false;
    }
    // sampling was successful
    return true;
  }

  void RejectionSampler::randomizeVars() const {
    // set the variables randomly within the box
    std::map<RooRealVar*,double> vals;
    fillRandomNormVector(vals);
    std::uniform_real_distribution<double> uniform(0,1);
    double rad = pow(uniform(this->random),1./this->variables.size());
    for(auto& var:this->variables){
      double range = (box <= 0 ? std::min(fabs(var.second - var.first->getMin()),fabs(var.second - var.first->getMax())) : box*var.first->getError() );
      double val = vals[var.first]*range*rad + var.second;
      var.first->setVal(val);
    }
  }
}
