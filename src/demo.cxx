#include <iostream>
#include <map>
#include <vector>
#include <exception>
#include <limits>
#include <chrono>
#include <ctime>

// ROOT includes
#include <TCanvas.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TRandom3.h>
#include <TGraph2D.h>
#include <TStyle.h>

// RooFit includes
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include <RooFitResult.h>
#include <RooRealVar.h>
#include <RooMinuit.h>
#include <RooAbsPdf.h>
#include <RooPlot.h>

#include <BlurRing/global.hpp>

void drawBlurRingPlot(RooAbsReal* nll, RooAbsCollection* args, const char* xname, const char* yname, bool relativeNorm){
  //  BlurRing::max_failcount = -1;
  //  BlurRing::max_failrate = -1;
  auto now = std::chrono::system_clock::now().time_since_epoch();
  unsigned int seed = now.count();
  auto contours = BlurRing::getContours(nll,args,
                                        BlurRing::Variable((RooRealVar*)args->find(xname),.02), //.1
                                        BlurRing::Variable((RooRealVar*)args->find(yname),.02), //.1
                                        {1,2},10,relativeNorm,0.2,true,1.,0);
  TString name(TString::Format("%s_%s_%s",nll->GetName(),xname,yname));
  TCanvas* c = new TCanvas(name,name);
  bool first = true;
  std::vector<int> colors = {kRed,kBlue};
  std::vector<int> colorsfirst = {kMagenta,kCyan};
  for(size_t i=contours.size(); i>0; --i){
    bool first_of_set = true;
    for(auto& graph:contours[i-1]){
      if(first_of_set){
        graph->SetLineColor(colorsfirst[i-1]);
	graph->SetLineStyle(2);
      } else {
        graph->SetLineColor(colors[i-1]);
	graph->SetLineStyle(1);      
      }
      first_of_set=false;
      if(first){
        graph->Draw("AC");
        graph->SetTitle("");
        graph->GetXaxis()->SetTitle(xname);
        graph->GetYaxis()->SetTitle(yname);
        c->Update();
      } else {
        graph->Draw("C");
      }
      first=false;
    }
  }
  c->SaveAs(TString::Format("%s_%s_%s_%s.pdf",nll->GetName(),xname,yname,relativeNorm ? "relNorm" : "absNorm"));
  delete c;
}

int main(){
  gStyle->SetOptStat(false);
  gStyle->SetOptTitle(false); 
  gStyle->SetLabelSize(0.05,"XYZ");
  gStyle->SetTitleSize(0.05,"XYZ");
  gStyle->SetTitleOffset(1.2,"Z");

  // Make dummy measurement 
  RooWorkspace w("w",1) ;

  // create regular model
  w.factory("SUM::model(n[0.3,0,0.75]*Gaussian::sig(v[-10,10],m[0,-5,5],w[2,0.1,4]),Uniform::bkg(v))") ;//n[0.3,0,0.5]
  RooAbsPdf* model = w.pdf("model");
  RooArgSet args(*w.var("n"),*w.var("w"),*w.var("m"));
  RooDataSet* data = model->generate(*w.var("v"),100) ; //1000
  RooAbsReal* nll = model->createNLL(*data) ;

  RooFitResult* r_orig = model->fitTo(*data,RooFit::Save()) ;

  // create hessian model
  RooAbsPdf* model_hessian = r_orig->createHessePdf(args);
  model_hessian->SetName("model_hessian") ;
  model_hessian->Print() ;
  w.import(*model_hessian) ;
  RooArgSet centralvals(*w.var("n_centralvalue"),*w.var("w_centralvalue"),*w.var("m_centralvalue"));
  RooDataSet* hessian_data = new RooDataSet("hessian_data","hessian data",centralvals);
  hessian_data->add(centralvals);
  RooAbsReal* nll_hessian =   model_hessian->createNLL(*hessian_data) ;

  //nll_hessian->Print();
  //hessian_data->Print();
  
  model_hessian->fitTo(*hessian_data);

  std::cout<<"creating hessian nll bluring plots"<<std::endl;
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"w","n",false);//n,w,m
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"m","n",false);//n,w,m
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"m","w",false);//n,w,m
  std::cout<<"creating nll bluring plots"<<std::endl;
  drawBlurRingPlot(nll,nll->getVariables(),"w","n",false);
  drawBlurRingPlot(nll,nll->getVariables(),"m","n",false);
  drawBlurRingPlot(nll,nll->getVariables(),"m","w",false);

  std::cout<<"creating hessian nll bluring plots"<<std::endl;
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"w","n",true);//n,w,m
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"m","n",true);//n,w,m
  drawBlurRingPlot(nll_hessian,nll_hessian->getVariables(),"m","w",true);//n,w,m
  std::cout<<"creating nll bluring plots"<<std::endl;
  drawBlurRingPlot(nll,nll->getVariables(),"w","n",true);
  drawBlurRingPlot(nll,nll->getVariables(),"m","n",true);
  drawBlurRingPlot(nll,nll->getVariables(),"m","w",true);  

  //std::cout << w.var("fbkg")->getVal() << std::endl;
  std::cout <<"m = " << w.var("m")->getVal() << std::endl;
  std::cout <<"n = " << w.var("n")->getVal() << std::endl;
  std::cout <<"w = " << w.var("w")->getVal() << std::endl; 
  
  TCanvas* c1 = new TCanvas("c1","c1",600,600);
  c1->SetRightMargin(0.05);
  c1->SetLeftMargin(0.15);
  RooPlot* frame = w.var("v")->frame() ;
  data->plotOn(frame) ;
  model->plotOn(frame) ;
  model->plotOn(frame,RooFit::Components("bkg"),RooFit::LineStyle(kDashed)) ;  
  frame->Draw() ;
  c1->SaveAs("model.pdf");

  //exit(0);
  
  r_orig->Print("v") ;


  TCanvas* c2 = new TCanvas("c2","c2",600,600) ;
  c2->SetRightMargin(0.2);
  TH2D* h = new TH2D("nll","",100,0.05,0.75,100,0.1,4) ;
  Double_t nllmin(1e30) ;
  for (int ix=0 ; ix<100 ; ix++) {
    for (int iy=0 ; iy<100 ; iy++) {
      double x = 0.05 + (ix+0.5)*(0.75-0.05)/100 ;
      double y = 0.1 + (iy+0.5)*(4-0.1)/100 ;
      w.var("n")->setVal(x) ;
      w.var("w")->setVal(y) ;
      w.var("m")->setVal(0);
      w.var("m")->setConstant(true);
      h->SetXTitle("n");
      h->SetYTitle("w");
      h->SetZTitle("nll");
      //w.var("m")->setVal(y) ;
      double nllval = nll->getVal() ;
      if (nllval<nllmin) nllmin=nllval ;
      h->Fill(x,y,nllval) ;
    }
  }
  h->SetMinimum(nllmin) ;
  h->SetMaximum(nllmin+100) ;
  h->Draw("cont1z") ;
  c2->SaveAs("nll_scan_n_w.pdf");
  w.var("m")->setConstant(false);

  TCanvas* c3 = new TCanvas("c3","c3",600,600) ;
  c3->SetRightMargin(0.2);
  TH2D* h3 = new TH2D("nll3","",100,0.05,0.75,100,-5,5) ;
  Double_t nllmin3(1e30) ;
  for (int ix3=0 ; ix3<100 ; ix3++) {
    for (int iy3=0 ; iy3<100 ; iy3++) {
      double x3 = 0.05 + (ix3+0.5)*(0.75-0.05)/100 ;
      double y3 = -5 + (iy3+0.5)*(5+5)/100 ;
      w.var("n")->setVal(x3) ;
      //w.var("w")->setVal(y3) ;
      w.var("m")->setVal(y3) ;
      w.var("w")->setVal(2);
      w.var("w")->setConstant(true);
      h3->SetXTitle("n");
      h3->SetYTitle("m");
      h3->SetZTitle("nll");
      double nllval3 = nll->getVal() ;
      if (nllval3<nllmin3) nllmin3=nllval3 ;
      h3->Fill(x3,y3,nllval3) ;
    }
  }
  h3->SetMinimum(nllmin3) ;
  h3->SetMaximum(nllmin3+100) ;
  h3->Draw("cont1z") ;
  c3->SaveAs("nll_scan_n_m.pdf");
  w.var("w")->setConstant(false);

  TCanvas* c4 = new TCanvas("c4","c4",600,600) ;
  c4->SetRightMargin(0.2);
  TH2D* h4 = new TH2D("nll4","",100,0.1,4,100,-5,5) ;
  Double_t nllmin4(1e30) ;
  for (int ix4=0 ; ix4<100 ; ix4++) {
    for (int iy4=0 ; iy4<100 ; iy4++) {
      double x4 = 0.1 + (ix4+0.5)*(4-0.1)/100 ;
      double y4 = -5 + (iy4+0.5)*(5+5)/100 ;
      //w.var("n")->setVal(x4) ;
      w.var("w")->setVal(x4) ;
      w.var("m")->setVal(y4) ;
      w.var("n")->setVal(0.3);
      w.var("n")->setConstant(true);
      h4->SetXTitle("w");
      h4->SetYTitle("m");
      h4->SetZTitle("nll");
      double nllval4 = nll->getVal() ;
      if (nllval4<nllmin4) nllmin4=nllval4 ;
      h4->Fill(x4,y4,nllval4) ;
    }
  }
  h4->SetMinimum(nllmin4) ;
  h4->SetMaximum(nllmin4+100) ;
  h4->Draw("cont1z") ;
  c4->SaveAs("nll_scan_w_m.pdf");
  w.var("n")->setConstant(false);
  
  w.var("n")->setConstant(kFALSE) ;
  w.var("w")->setConstant(kFALSE) ;
  w.var("m")->setConstant(kFALSE) ;


    
  RooMinuit m(*nll_hessian) ;
  m.optimizeConst(1) ;
  m.setVerbose(1) ;

  ((RooRealVar*)nll_hessian->getVariables()->find("n"))->setVal(0.4) ;

  m.migrad() ;
  m.hesse() ;

  RooFitResult* r_hesse = m.save() ;
  
  // Plot Hessian likelihood
  RooRealVar* myn = (RooRealVar*) nll_hessian->getVariables()->find("n") ;
  RooRealVar* myw = (RooRealVar*) nll_hessian->getVariables()->find("w") ;
  RooRealVar* mym = (RooRealVar*) nll_hessian->getVariables()->find("m") ;
  TCanvas* c5 = new TCanvas("c5","c5",600,600) ;
  c5->SetRightMargin(0.2);
  TH2D* h5 = new TH2D("nll_h","",100,0.05,0.75,100,0.1,4) ;
  nllmin = 1e30 ;
  for (int ix=0 ; ix<100 ; ix++) {
    for (int iy=0 ; iy<100 ; iy++) {
      double x = 0.05 + (ix+0.5)*(0.75-0.05)/100 ;
      double y = 0.1 + (iy+0.5)*(4-0.1)/100 ;
      myn->setVal(x) ;
      myw->setVal(y) ;
      mym->setVal(0);
      mym->setConstant(true);
      h5->SetXTitle("n");
      h5->SetYTitle("w");
      h5->SetZTitle("nll");
      double nllval = nll_hessian->getVal() ;
      if (nllval<nllmin) nllmin=nllval ;
      h5->Fill(x,y,nllval) ;
    }
  }
  std::cout << "nllmin = " << nllmin << std::endl ;
  h5->SetMinimum(nllmin) ;
  h5->SetMaximum(nllmin+100) ;
  h5->Draw("cont1z") ;
  c5->SaveAs("nll_hessian_scan_n_w.pdf");
  mym->setConstant(false);

  TCanvas* c3h = new TCanvas("c3h","c3h",600,600) ;
  c3h->SetRightMargin(0.2);
  TH2D* h3h = new TH2D("nll3_h","",100,0.05,0.75,100,-5,5) ;
  Double_t nllmin3h(1e30) ;
  for (int ix3=0 ; ix3<100 ; ix3++) {
    for (int iy3=0 ; iy3<100 ; iy3++) {
      double x3 = 0.05 + (ix3+0.5)*(0.75-0.05)/100 ;
      double y3 = -5 + (iy3+0.5)*(5+5)/100 ;
      myn->setVal(x3) ;
      //w.var("w")->setVal(y3) ;
      mym->setVal(y3) ;
      myw->setVal(2);
      myw->setConstant(true);
      h3h->SetXTitle("n");
      h3h->SetYTitle("m");
      h3h->SetZTitle("nll");
      double nllval3 = nll_hessian->getVal() ;
      if (nllval3<nllmin3h) nllmin3h=nllval3 ;
      h3h->Fill(x3,y3,nllval3) ;
    }
  }
  h3h->SetMinimum(nllmin3h) ;
  h3h->SetMaximum(nllmin3h+100) ;
  h3h->Draw("cont1z") ;
  c3h->SaveAs("nll_hessian_scan_n_m.pdf");
  myw->setConstant(false);

  TCanvas* c4h = new TCanvas("c4h","c4h",600,600) ;
  c4h->SetRightMargin(0.2);
  TH2D* h4h = new TH2D("nll4_h","",100,0.1,4,100,-5,5) ;
  Double_t nllmin4h(1e30) ;
  for (int ix4=0 ; ix4<100 ; ix4++) {
    for (int iy4=0 ; iy4<100 ; iy4++) {
      double x4 = 0.1 + (ix4+0.5)*(4-0.1)/100 ;
      double y4 = -5 + (iy4+0.5)*(5+5)/100 ;
      //w.var("n")->setVal(x4) ;
      myw->setVal(x4) ;
      mym->setVal(y4) ;
      myn->setVal(0.3);
      myn->setConstant(true);
      h4h->SetXTitle("w");
      h4h->SetYTitle("m");
      h4h->SetZTitle("nll");
      double nllval4 = nll_hessian->getVal() ;
      if (nllval4<nllmin4h) nllmin4h=nllval4 ;
      h4h->Fill(x4,y4,nllval4) ;
    }
  }
  h4h->SetMinimum(nllmin4h) ;
  h4h->SetMaximum(nllmin4h+100) ;
  h4h->Draw("cont1z") ;
  c4h->SaveAs("nll_hessian_scan_w_m.pdf");
}
