#include <BlurRing/global.hpp>

#include <vector>

#include <TGraph.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TROOT.h>

#include <RooRealVar.h>
#include <RooAbsCollection.h>
#include <RooMinimizer.h>

#include <BlurRing/GeneralFunctionSampler.hpp>
#include <BlurRing/RejectionSampler.hpp>
#include <BlurRing/GaussSampler.hpp>

#define inf std::numeric_limits<double>::infinity()

unsigned int max_failrate = 100; 
unsigned int max_failcount = 1000;

namespace {

  typedef std::map<RooRealVar*,BlurRing::Variable> Snapshot;

  ::Snapshot getSnapshot(RooAbsCollection* args){
    RooFIter itr(args->fwdIterator());
    TObject* obj;
    std::map<RooRealVar*,BlurRing::Variable> snapshot;
    while((obj = itr.next())){
      if(!obj) continue;
      RooRealVar* t = dynamic_cast<RooRealVar*>(obj);
      if(!t) continue;
      snapshot[t]=BlurRing::Variable(t);
    }
    return snapshot;
  }

  void loadSnapshot(const ::Snapshot& snapshot){
    for(auto it:snapshot){
      it.first->setVal(it.second.getMinimum());
      it.first->setError(it.second.getError());      
    }
  }
  
  
  std::vector<RooRealVar*> getVariables(RooAbsCollection* args, const std::vector<RooAbsReal*>& skip){
    // acquire a vector of variables from a collection, excluding the given list
    RooFIter itr(args->fwdIterator());
    TObject* obj;
    std::vector<RooRealVar*> variables;
    while((obj = itr.next())){
      if(!obj) continue;
      RooRealVar* t = dynamic_cast<RooRealVar*>(obj);
      if(!t) continue;
      bool skipped=false;
      for(auto v:skip){
        if(v==t){
          skipped = true;
        }
      }
      if(!skipped){
        variables.push_back(t);
      }
    }
    return variables;
  }
  
  std::vector<RooRealVar*> getVariables(RooAbsReal* func, const std::vector<RooAbsReal*>& skip){
    // acquire a vector of variables from a function, excluding the given list
    RooArgSet* components(func->getComponents());
    auto retval = ::getVariables(components,skip);
    delete components;
    return retval;
  }
}


namespace BlurRing {

  std::vector<std::vector<TGraph*>> getContourSet(RooAbsReal* func,double offset,
                                                  const BlurRing::Variable& x,
                                                  const BlurRing::Variable& y,
                                                  const std::vector<double>& contourVals){
    // generate a set of contours corresponding to the currently set coordinates
    if(contourVals.size() == 0){
      throw std::runtime_error("cannot make plot: empty list of contour vals!");
    }
    // create a helper
    const double xmin = x.getMinimum() - x.getStep()*(int)((x.getMinimum() - x.getMin())/x.getStep());
    const double ymin = y.getMinimum() - y.getStep()*(int)((y.getMinimum() - y.getMin())/y.getStep());
    const size_t nbinx = (x.getMax()-xmin)/x.getStep();
    const size_t nbiny = (y.getMax()-ymin)/y.getStep();
    TH2F* helper = new TH2F("helper","helper",
                            nbinx,xmin,xmin+x.getStep()*nbinx,
                            nbiny,ymin,ymin+y.getStep()*nbiny);
    helper->SetDirectory(NULL);
    // fill the helper with values of the function.
    double zmin=inf,zmax=-inf;
    for(size_t ix=0; ix<nbinx; ++ix){
      const double xval = xmin + ix*x.getStep();
      x.getVar()->setVal(xval);
      for(size_t iy=0; iy<nbiny; ++iy){
        const double yval = ymin + iy*y.getStep();
        y.getVar()->setVal(yval);
        const double zval = func->getVal() - offset;
        if(zval>zmax) zmax=zval;
        if(zval<zmin) zmin=zval;
        helper->SetBinContent(ix+1,iy+1,zval);
      }
    }
    std::vector<std::vector<TGraph*>> retval;
    // if the range of the graph is empty, there is nothing to draw
    if(zmin==zmax){
      std::cout << "values are all equal, cannot draw contour" << std::endl;
      return retval;
    }
    double maxcont = contourVals[contourVals.size()-1];
    if(maxcont < zmin ){
      std::cout << "values outside of contour range. highest contour is " << maxcont << ", but lowest value is " << zmin << std::endl;
      return retval;
    }
    // loop over all requested contours, generate them from the graph and add them to the list
    helper->SetContour(contourVals.size(),&contourVals[0]);
    TCanvas* helpercanvas = new TCanvas("helpercanvas","helpercanvas");
    helpercanvas->cd();
    helper->Draw("CONTZLIST");
    helpercanvas->Update();
    TObjArray* contourList = dynamic_cast<TObjArray*>(gROOT->GetListOfSpecials()->FindObject("contours"));
    if(!contourList){
      std::cout << "unable to retrieve contours " << std::endl;
      delete helpercanvas;
      return retval;
    }
    TIter iObj(contourList);
    int i=0; 
    while (TObject* obj = iObj()) {
      double contour = contourVals[i];
      std::vector<TGraph*> graphs;
      if(contour < zmin || contour > zmax){
        retval.push_back(graphs);
      } else {
        TList* contLevel = dynamic_cast<TList*>(obj);
        if(!contLevel) continue;
        TIter next(contLevel);
        TObject* gobj;
        while((gobj = next())){
          TGraph* gorig = dynamic_cast<TGraph*>(gobj);
          if(!gorig) continue;
          TGraph* g = (TGraph*)(gorig->Clone());
          graphs.push_back(g);
        }
        retval.push_back(graphs);
      }
      ++i;
    }
    delete helpercanvas;
    return retval;
  }

  unsigned int max_failrate = 100; // maximum number of failed samples per requested sample
  unsigned int max_failcount = 10000; // maximum number of failed samples in total

  std::vector<std::vector<TGraph*>> getContours(RooAbsReal* func,RooAbsCollection* args,
                                                const BlurRing::Variable& x,
                                                const BlurRing::Variable& y,
                                                const std::vector<double>& contourVals, int nSamples, bool relNorm,
                                                double funcmax, bool logarithmic, double box, unsigned int seed){
    // generate ellipses corresponding to the given contour values the
    // return value is a vector of vectors, the outer index
    // corresponding to the index of the contour value, the inner one
    // the index roughly equal to the number of samples -- this is no
    // true equality, because in the case of double minima, there
    // might be several ellipses per sample.
    // this version samples from the likelihood itself using rejection sampling, which can be slow..

    // save the initial values
    auto snapshot = ::getSnapshot(args);

    // we need a sampler to generate samples from the function
    BlurRing::RejectionSampler sampler(func,::getVariables(args,{x.getVar(),y.getVar()}),funcmax,logarithmic,box);
    sampler.setSeed(seed);

    std::vector<std::vector<TGraph*>> ellipses = BlurRing::getContours(func,sampler,x,y,contourVals,nSamples,relNorm);
    
    // set everything back to the initial values
    ::loadSnapshot(snapshot);

    return ellipses;
  }

  std::vector<std::vector<TGraph*>> getGaussianContours(RooAbsReal* func,RooAbsCollection* args,
                                                        const BlurRing::Variable& x,
                                                        const BlurRing::Variable& y,
                                                        const std::vector<double>& contourVals, int nSamples, bool relNorm,
                                                        double box, unsigned int seed){
    // generate ellipses corresponding to the given contour values the
    // return value is a vector of vectors, the outer index
    // corresponding to the index of the contour value, the inner one
    // the index roughly equal to the number of samples -- this is no
    // true equality, because in the case of double minima, there
    // might be several ellipses per sample.
    // this version creates samples from a multi-dimensional gaussian instead of from the likelihood itself, which is much faster.

    // save the initial values
    auto snapshot = ::getSnapshot(args);

    // we need a sampler to generate samples from the function
    BlurRing::GaussSampler sampler(::getVariables(args,{x.getVar(),y.getVar()}),box);
    sampler.setSeed(seed);

    std::vector<std::vector<TGraph*>> ellipses = BlurRing::getContours(func,sampler,x,y,contourVals,nSamples,relNorm);
    
    // set everything back to the initial values
    ::loadSnapshot(snapshot);

    return ellipses;
  }  

  std::vector<std::vector<TGraph*>> getContours(RooAbsReal* func,
                                                const BlurRing::Sampler& sampler,
                                                const BlurRing::Variable& x,
                                                const BlurRing::Variable& y,
                                                const std::vector<double>& contourVals, int nSamples, bool relNorm){
    // generate ellipses corresponding to the given contour values the
    // return value is a vector of vectors, the outer index
    // corresponding to the index of the contour value, the inner one
    // the index roughly equal to the number of samples -- this is no
    // true equality, because in the case of double minima, there
    // might be several ellipses per sample.
    
    // counters for the rejection sampling
    int nfails = 0;
    int ok = 0;
    // the return value
    std::vector<std::vector<TGraph*>> ellipses;
    // set the x and y to the function minimum
    x.toMinimum();
    y.toMinimum();
    double minVal = func->getVal();
    // first, get the contours for the minimum
    std::vector<std::vector<TGraph*>> contours(BlurRing::getContourSet(func,minVal,x,y,contourVals));
    // we will have one vector of TGraph per contour value    
    if(contours.size() != contourVals.size()) return ellipses;
    for(size_t i=0; i<contourVals.size(); ++i){
      ellipses.push_back(std::vector<TGraph*>());
    }
    // take the contours and push it into the list
    for(size_t i=0; i<contourVals.size(); ++i){
      for(TGraph* g:contours[i]){
        ellipses[i].push_back(g);
      }
    }

    // create a minimizer in case a relative normalization was requested
    RooMinimizer minim(*func);

    auto snapshot = ::getSnapshot(func->getVariables());
    
    // loop until we have sufficiently many samples generated
    while(ok < nSamples){
      // escape if we have too many failed samples
      if(nfails >= std::max(max_failrate*nSamples,max_failcount)){
        TString errmsg(TString::Format("encountered %d failed samples after only %d successful ones",nfails,ok));
        errmsg.Append(", please find a better box for your function!");
        throw std::runtime_error(errmsg.Data());
      }
      // set the x and y to the function minimum
      ::loadSnapshot(snapshot);

      // generate a sample randomly
      if(!sampler.generate()){
        // point was sampled, but rejected. continue.
        nfails++;
        continue;
      }

      double thisMin = minVal;
      // calculate the relative normalization if requested
      if(relNorm){
        sampler.setConst(true);
        minim.minimize("Minuit2","Migrad");
        thisMin = func->getVal();
        sampler.setConst(false);
      }
      // set the x and y to the function minimum
      x.toMinimum();
      y.toMinimum();

      // make the contours corresponding to the contour values from the function
      std::vector<std::vector<TGraph*>> contours(BlurRing::getContourSet(func,thisMin,x,y,contourVals));
      if(contours.size() == 0){
        // point was accepted, but graph does not contain any values in the intended contour range. continue.
        nfails++;
        continue;
      }
      // at this point, we know the curve is usable
      ok++;
      // all we need to do now is take the contours and push it into the list
      for(size_t i=0; i<contourVals.size(); ++i){
        for(TGraph* g:contours[i]){
          ellipses[i].push_back(g);
        }
      }
    }

    // return all the acquired ellipses
    return ellipses;
  }
}
