#include "BlurRing/Sampler.hpp"

#include "RooAbsReal.h"
#include "RooRealVar.h"

namespace BlurRing {

  Sampler::Sampler(const std::vector<RooRealVar*>& vars, double b) : box(b)
  {
    // default constructor
    for(auto& v:vars){
      variables[v]=v->getVal();
    }
  }
  
  void Sampler::setSeed(int seed){
    // set the random seed of the sampler
    this->random.seed(seed);
  }

  void Sampler::printVars() const {
    // print the variables and their current values
    for(auto& it:this->variables){
      std::cout << it.first->GetName() << " = " << it.first->getVal();
      if(it.first->isConstant()) std::cout << " (const)";
      std::cout << std::endl;
    }
  }

  void Sampler::setConst(bool constval) const {
    // set all parameter constant / floating
    for(auto& it:this->variables){
      it.first->setConstant(constval);
    }
  }
  
  void Sampler::fillRandomNormVector(std::map<RooRealVar*,double>& vec) const {
    // set a vector to a random normalized coordinate in parameter space
    std::normal_distribution<double> gauss(0,1);
    double ssq(0);
    for(auto& var:this->variables){
      double val = gauss(this->random);
      vec[var.first] = val;
      ssq += val*val;
    }
    double norm = 1./sqrt(ssq);
    for(auto& var:vec){
      var.second *= norm;
    }
  }
}


