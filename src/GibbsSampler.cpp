#include "BlurRing/GibbsSampler.hpp"

#include "RooAbsReal.h"
#include "RooRealVar.h"

namespace BlurRing {

  GibbsSampler::~GibbsSampler(){
    // default destructor
  }

  void GibbsSampler::configure(int burnIn, int _skip){
    // setup the sampler
    for(size_t i=0; i<burnIn; ++i){
      // burn-in
      this->generate();
    }
    this->skip = _skip;
  }
  
  bool GibbsSampler::generate() const {
    // generate a sample
    if(this->skip < 0){
      throw std::runtime_error("call configure(burnIn,skip) before using the GibbsSampler!");
    }
    for(size_t i=0; i<skip; ++i){
      this->randomizeVars();
    }
    double randfuncval = this->getRandomFunctionValue();
    double funcval = func->getVal()-this->getMinimumFunctionValue();
    if(funcval < randfuncval){
      // if the sample is not accepted, retry
      return false;
    }
    // sampling was successful
    return true;
  }

  void GibbsSampler::randomizeVars() const {
    // set the variables randomly within the box
    for(auto& var:this->samplers){
      double sample = var.second.generate();
      var.first->setVal(sample);
    }
  }

  void GibbsSampler::initialize(){
    // initialization function to be called from the constructor
    for(auto it:variables){
      std::vector<RooRealVar*> v = {it.first};
      this->samplers.emplace(it.first,RejectionSampler(this->func,v,this->funcmax,this->logarithmic,this->box));
    }
  }
  
}
