#include "BlurRing/Variable.hpp"

#include "RooRealVar.h"

namespace BlurRing {
  
  Variable::Variable(RooRealVar*  _var, double _min, double _max, double _step) :
    var(_var),
    minVal(_var->getVal()),
    min(std::max(_min,var->getMin())),
    max(std::min(_max,var->getMax())),
    error(var->getError()),
    step(_step)
  {}
  
  Variable::Variable(RooRealVar*  _var, double _step) :
    var(_var),
    minVal(_var->getVal()),
    min(var->getMin()),
    max(var->getMax()),
    error(var->getError()),
    step(_step)
  {}

  Variable::Variable(RooRealVar*  _var) :
    var(_var),
    minVal(_var->getVal()),
    min(var->getMin()),
    max(var->getMax()),
    error(var->getError()),
    step(0)
  {}

  Variable::Variable() {};
  
  void Variable::toMinimum() const {
    var->setVal(minVal);
  }

  RooRealVar* Variable::getVar() const {
    return this->var;
  }
  double Variable::getMinimum() const {
    return this->minVal;
  }
  double Variable::getMin() const {
    return this->min;
  }
  double Variable::getMax() const {
    return this->max;
  }
  double Variable::getStep() const {
    return this->step;
  }
  double Variable::getError() const {
    return this->error;
  }  

}
