#include "BlurRing/GeneralFunctionSampler.hpp"

#include "RooAbsReal.h"
#include "RooRealVar.h"

namespace BlurRing {

  GeneralFunctionSampler::GeneralFunctionSampler(RooAbsReal* f,
                           const std::vector<RooRealVar*>& vars,
                           double fmax,
                           bool log,
                           double b) : Sampler(vars,b),
                                       func(f),funcmin(f->getVal()),funcmax(fmax),logarithmic(log)
  {
    // default constructor
    initialize();
  }

  GeneralFunctionSampler::~GeneralFunctionSampler(){
    // default destructor
  }

  void GeneralFunctionSampler::initialize(){
    // initialization function to be called from the constructor (empty)
  }  
  
  double GeneralFunctionSampler::getMinimumFunctionValue() const {
    // return the minimum function value
    return this->funcmin;
  }
    
  double GeneralFunctionSampler::getRandomFunctionValue() const {
    // roll the function value. if logarithmic (Nll), use
    // exponential distribution, otherwise (LH) use uniform
    if(this->logarithmic){
      std::exponential_distribution<double> exponential(this->funcmax);
      return exponential(this->random);
    } else {
      std::uniform_real_distribution<double> uniform(0.,this->funcmax);
      return uniform(this->random);
    }
  }
    
 

}


